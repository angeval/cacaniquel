package com.company;

import java.util.ArrayList;

public class CalculadoraPontos {
    private int pontos=0;
    ArrayList<Simbolos> simbolos = new ArrayList<>();

    public int calcularPontos(ArrayList<Simbolos> simbolos){
        boolean todosIguais = true;

        for (int i=0;i<simbolos.size();i++) {
            pontos +=simbolos.get(i).getValue();
            if (i+1 < simbolos.size()) {
                if(!simbolos.get(i).equals(simbolos.get(i+1))) {
                    todosIguais = false;
                }
            }
        }
        if (todosIguais){
            pontos *= Constantes.BONUS;
        }
        return pontos;
    }
}
