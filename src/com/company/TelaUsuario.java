package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class TelaUsuario {
    public boolean fica=true;
    Scanner scanner = new Scanner(System.in);
    ArrayList<Double> valores = new ArrayList<>();
    HashMap<String, String> formas = new HashMap<>();

    public void informarDados(){
        System.out.println("Informe [S] para terminar o jogo OU Qualquer outra tecla para continuar... ");
        fica=true;
        String sair = scanner.next();

        if (sair.equals("S")||sair.equals("s")) {
                fica= false;
        }
    }

    public boolean validarDados() {
        if (!fica) {
            System.out.println("O jogo acabou. ");
        }
        return fica;
    }

    public void imprimir(String mensagem) {
        System.out.println(mensagem);
    }
}
