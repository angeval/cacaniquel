package com.company;

import java.util.Random;

public class Slot {
    private Simbolos slot;

    public Simbolos sortear() {

        Random random = new Random();
        int index = random.nextInt(Simbolos.values().length);
        this.slot = Simbolos.values()[index];
        return this.slot;
    }
}
