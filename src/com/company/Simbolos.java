package com.company;

public enum Simbolos {
        BANANA(10),
        FRAMBOESA(50),
        MOEDA(100),
        SETE(300)
        ;

        private int ponto;
        Simbolos(int i) {
            this.ponto = i;
        }

        public int getValue() {
            return ponto;
        }

}
