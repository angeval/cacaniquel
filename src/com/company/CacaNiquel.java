package com.company;

import java.util.HashMap;

public class CacaNiquel {
    private int maiorPontuacao = 0;

    public int jogar(){
        Sorteador sorteador = new Sorteador();
        CalculadoraPontos calc = new CalculadoraPontos();
        TelaUsuario tela = new TelaUsuario();
        int pontos = calc.calcularPontos(sorteador.sortearSlots());
        tela.imprimir("Pontos dessa jogada "+pontos);
        if (pontos > maiorPontuacao){
            maiorPontuacao = pontos;
        }
        return maiorPontuacao;
    }
}
