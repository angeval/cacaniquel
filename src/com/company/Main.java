package com.company;

public class Main {

    public static void main(String[] args) {
            CacaNiquel cacaNiquel = new CacaNiquel();
            TelaUsuario tela = new TelaUsuario();

            while (tela.validarDados()){
                int maiorPontuacao = cacaNiquel.jogar();
                tela.imprimir("Maior pontuação: " + maiorPontuacao);
                tela.informarDados();
            }
        }
}
